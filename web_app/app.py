from flask import Flask, render_template


def create_app():
    app = Flask(__name__)

    # ambil config dari file settings.py
    app.config.from_pyfile('settings.py')

    @app.route("/")
    def hello():
        return render_template("index.html")

    @app.route("/about")
    def about():
        return render_template("about.html")

    @app.route("/testdb")
    def testdb():
        return "testdb"

    return app
