# python image from docker hub
FROM python:2.7-slim

# meta data docker
MAINTAINER almatins <almatin.siswanto@gmail.com>

# memastikan repository update di docker image
RUN apt-get update && apt-get install -qq -y \
    build-essential libpq-dev --no-install-recommends

# environment variable install path di docker image
ENV INSTALL_PATH /web_app

# create path di docker image jika belum ada
RUN mkdir -p $INSTALL_PATH

# docker image working directory
WORKDIR $INSTALL_PATH

# copy requirements.txt to docker image
COPY requirements.txt requirements.txt

# run requirements.txt and install requirements yang ada di sana
RUN pip install -r requirements.txt

# copy current directory to working dir docker image
COPY . .

# jalankan gunicorn di port 8000 dengan command package web_app, module app, function create_app
CMD gunicorn -b 0.0.0.0:8000 --access-logfile - "web_app.app:create_app()"